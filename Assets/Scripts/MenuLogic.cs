﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.LogicScripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class MenuLogic : MonoBehaviour
    {
        public Image MenuPanel;
        public Image StagesPanel;

        public Button PlayButton;
        public Button SettingsButton;
        public Button Level1Button;
        public Button Level2Button;
        public Button Level3Button;

        // Stars Level 1
        public Image Star11;
        public Image Star12;
        public Image Star13;
        
        // Stars Level 1
        public Image Star21;
        public Image Star22;
        public Image Star23;
        
        // Stars Level 1
        public Image Star31;
        public Image Star32;
        public Image Star33;

        public Sprite StarActiveSprite;

        public Button BackButton;

        public Image SettingsOptions;

        private List<Image> _stageItems;
        private GameLogic _game;

        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            _game = GameLogic.Instance;

            _stageItems = StagesPanel.GetComponentsInChildren<Image>().ToList();
            _stageItems.Add(Level1Button.GetComponent<Image>());
            _stageItems.Add(Level2Button.GetComponent<Image>());
            _stageItems.Add(Level3Button.GetComponent<Image>());
            _stageItems.AddRange(Level1Button.GetComponentsInChildren<Image>());
            _stageItems.AddRange(Level2Button.GetComponentsInChildren<Image>());
            _stageItems.AddRange(Level3Button.GetComponentsInChildren<Image>());
            PlayButton.onClick.AddListener(Play);
            SettingsButton.onClick.AddListener(Settings);
            Level1Button.onClick.AddListener(Level1);
            Level2Button.onClick.AddListener(Level2);
            Level3Button.onClick.AddListener(Level3);
            BackButton.onClick.AddListener(Back);
            SetStars();
            if (_game.InMenu) Play();
        }

        private void SetStars()
        {
            var starImages = new[]
            {
                new[] {Star11, Star12, Star13}, 
                new[] {Star21, Star22, Star23}, 
                new[] {Star31, Star32, Star33}
            };

            for (int i = 0; i < 3; i++)
            {
                _game.SetStage(i + 1);
                var stars = _game.GetStars(_game.BestScore());
                var levelStars = starImages[i];
                switch (stars)
                {
                    case 1:
                        levelStars[0].sprite = StarActiveSprite;
                        break;
                    case 2:
                        levelStars[1].sprite = StarActiveSprite;
                        goto case 1;
                    case 3:
                        levelStars[3].sprite = StarActiveSprite;
                        goto case 2;
                }
            }
            
        }

        private void Play()
        {
            _game.InMenu = true;
            foreach (var item in _stageItems)
            {
                item.gameObject.SetActive(true);
            }
        }

        private void Settings()
        {
            SettingsOptions.gameObject.SetActive(!SettingsOptions.gameObject.activeSelf);
        }

        private void Back()
        {
            _game.InMenu = false;
            foreach (var item in _stageItems)
            {
                item.gameObject.SetActive(false);
            }
        }

        private void Level1()
        {
            _game.SetStage(1);
            SceneManager.LoadScene(1);
        }

        private void Level2()
        {
            _game.SetStage(2);
            SceneManager.LoadScene(1);
        }

        private void Level3()
        {
            _game.SetStage(3);
            SceneManager.LoadScene(1);
        }
    }
}
