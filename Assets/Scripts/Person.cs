﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.LogicScripts;

public class Person : MonoBehaviour {

    public float WaitTime = 2f;
    public float WaitTimeShoot = 0.8f;
    
    private Animator animator;
    private float MaxDistance = 50f;
    private float SafeDistance = 300f;
    private float lockPositionY = 0f;
    private System.Random ran = new System.Random();
    private RaycastHit hit;
    private float WaitingTime = 0f;

    private bool shooting = false;

    private GameObject BirdObject;

    public int cantHitted = 0;

    void Awake() {
        //GetComponent<Collider>().isTrigger = true;
        animator = GetComponent<Animator>();
        animator.Play("WalkState");

        animator.SetInteger("TypeWalk", ran.Next(0, 7));
        animator.SetInteger("TypeScared", ran.Next(0, 2));
        animator.SetInteger("TypeRun", ran.Next(0, 5));
        animator.SetInteger("TypeUpset", ran.Next(0, 2));

    }

    void Start() {
        BirdObject = GameObject.FindGameObjectWithTag("Player");
        GameLogic game = GameLogic.Instance;
        int StageNumber = game.StageNumber();
        WaitTimeShoot /= StageNumber;
    }

    void Update() {
        TranslateZ();

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Turn Right") || animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
            animator.SetBool("SomethingInFront", false);
        }

        hit = new RaycastHit();
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, MaxDistance)) {
            string tag = GetGameObjectTag(hit.collider.gameObject);
            if (tag.Equals("Person") || tag.Equals("Building") || tag.Equals("Barrier")) {
                if (!WasShooted()) {
                    animator.Play("Turn Right");
                    animator.SetBool("SomethingInFront", false);
                }
            } else if (tag.Equals("Car")) {
                if (!WasShooted()) {
                    if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
                        if (Time.time - WaitingTime < WaitTime) {
                            animator.Play("Turn Right");
                        } else {
                            animator.Play("Idle");
                            animator.SetBool("SomethingInFront", true);
                        }
                    }
                }
            }

            WaitingTime = Time.time;
        }

        if (cantHitted > 3 && !shooting && Vector3.Distance(gameObject.transform.position, BirdObject.transform.position) <= SafeDistance) {
            StartCoroutine(Shoot());
        }
    }

    private void TranslateZ() {
        Vector3 position = transform.position;
        position.y = lockPositionY;
        transform.position = position;

        if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Walk")){
            transform.Translate(Vector3.forward * Time.timeScale / 4f);
        }
    }

    void OnCollisionEnter(Collision collision) {
        Collider collider = collision.collider;
        string tag = collider.tag;
        if (collider.gameObject.transform.parent != null && tag.Equals("Untagged")) {
            tag = collider.gameObject.transform.parent.tag;
        }

        if (tag.Equals("Poop")) {
            cantHitted += 1;
            if (cantHitted > 3) {
                animator.Play("UpsetState");
                Shoot();
                //collider.gameObject.GetComponent<Animator>().Play("Fall " + ran.Next(1, 3));
            }
        }

        if (tag.Equals("Building") || tag.Equals("Person") || tag.Equals("Car") || tag.Equals("Barrier")) {
            if (!WasShooted() || tag.Equals("Building") || tag.Equals("Barrier")) {
                animator.Play("UpsetState");
                animator.SetBool("SomethingInFront", true);
                transform.Translate(0f, 0f, -2f);
            }
        } else {
            //animator.SetBool("SomethingInFront", false);
        }
    }

    private string GetGameObjectTag (GameObject rayObject) {
        string tag = rayObject.tag;
        if (rayObject.transform.parent != null && tag.Equals("Untagged")) {
            tag = rayObject.transform.parent.tag;
        }
        return tag;
    }

    private IEnumerator Shoot() {
        shooting = true;

        animator.Play("Pick Up");

        gameObject.transform.LookAt(BirdObject.transform);

        Object prefab = Resources.Load("People/Rock", typeof(GameObject));
        GameObject clone = Instantiate(prefab, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        clone.tag = "Rock";
        clone.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * 1000f, ForceMode.Impulse);
        Destroy(clone, 3);

        yield return new WaitForSeconds(WaitTimeShoot);

        shooting = false;
    }

    private bool WasShooted() {
        return GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Shooted");
    }
}
