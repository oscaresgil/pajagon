﻿using Assets.Scripts.LogicScripts;
using UnityEngine;

namespace Assets.Scripts
{
    public class Poop : MonoBehaviour {

        private AudioClip hitSound, shootSound;
        private GameLogic gameLogic;
        private System.Random ran = new System.Random();
        private AudioSource source;

        private float SplashRange = 1f;
        private float MinScale = 1f;
        private float MaxScale = 2f;

        void Awake() {
            source = gameObject.AddComponent<AudioSource>();
            hitSound = Resources.Load("Sounds/PoopHit") as AudioClip;
            shootSound = Resources.Load("Sounds/PoopShoot") as AudioClip;
        }

        void Start(){
            gameLogic = GameLogic.Instance;
            source.PlayOneShot(shootSound, 1f);
        }

        void OnCollisionEnter(Collision collision){
            Collider collider = collision.collider;
            source.PlayOneShot(hitSound, 1f);

            string tag = collider.tag;
            if (tag == "Person") {
                //gameLogic.CantPersonsPooped += 1;
                gameLogic.Score(gameLogic.Score() + 50);
                collider.gameObject.GetComponent<Animator>().Play("Scared " + ran.Next(1, 3));

            } else if (tag == "Car") {
                //gameLogic.CantCarsPooped += 1;
                //gameLogic.Points += 25;
                gameLogic.Score(gameLogic.Score() + 25);
            }

            Destroy(gameObject, 0.7f);
        }
    }
}
