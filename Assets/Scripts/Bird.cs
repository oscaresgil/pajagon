﻿using Assets.Scripts.LogicScripts;
using JetBrains.Annotations;
using UnityEngine;
using Assets.Standard_Assets.CrossPlatformInput.Scripts;
using System.Collections;

namespace Assets.Scripts
{
    public class Bird : MonoBehaviour 
    {

        public float RotationSpeed;
		public float StepZ, MinStepZ, MaxStepZ, StepFly, MaxStepY, MinStepY;
        public float MaximumHeight, MinimumHeight;

        private float ActualRotationZ = 0;
        private float StepY = 0f, Step = 0.1f, Step2 = 0.1f;
		private readonly Vector3 _scaleVector = new Vector3(5f,5f,5f);
        private GameLogic _gameLogic;
        private GameObject _parentPoop;
		private bool facingUp = false;

        private Vector3 wantedDeadZone;
        private Matrix4x4 calibrationMatrix;
        private Vector3 accelerometer;

        private bool shooting = false;
        private float WaitShoot = 0.5f;

        [UsedImplicitly]
        private void Awake() {
            _gameLogic = GameLogic.Instance;
            _parentPoop = new GameObject("Poops");
            calibrarteAccelerometer();

            GameLogic game = GameLogic.Instance;
            int StageNumber = game.StageNumber();
            if (StageNumber == 1) {
                StepZ = 1f;
                MaxStepZ = 1.5f;
            } else if (StageNumber == 2) {
                StepZ = 1.5f;
                MaxStepZ = 2f;
            } else {
                StepZ = 2f;
                MaxStepZ = 2.5f;
            }
        }

        [UsedImplicitly]
        private void Update()
		{

            accelerometer = getAccelerometer(Input.acceleration);

            MovementZ();
            MovementX(accelerometer.x);
            MovementY(accelerometer.y);

            Vector3 actualAngles = transform.eulerAngles;
            actualAngles.z = ActualRotationZ;
            transform.eulerAngles = actualAngles;

            if (transform.position.y < MinimumHeight) {
                Vector3 position = transform.position;
                position.y = MinimumHeight + 0.2f;
                transform.position = position;
            }

            if (CrossPlatformInputManager.GetButtonDown ("Fly")) {
				Fly ();
			}
            /*if (Input.GetKeyDown(KeyCode.Space)) {
                Fly();
            }
            */
            if (CrossPlatformInputManager.GetButtonDown ("Shoot") && !shooting) {
                StartCoroutine(Shoot ());
			}
            /*if (Input.GetKeyDown(KeyCode.P) && !shooting){
                StartCoroutine(Shoot());
            }*/
        }

        private void MovementZ() 
        {
			Vector3 angles = transform.rotation.eulerAngles;
			if (angles.x > 250f && angles.x < 330f) {
				facingUp = true;
			} else {
				facingUp = false;
			}

            if (StepY > MinimumHeight) {
                StepY = GetStep(StepY, MinStepY, MaxStepY, Step, Step2, facingUp);
            } else {
                StepY = 0f;
                //transform.position = new Vector3(transform.position.x, MinimumHeight, transform.position.z);
            }
            StepY = GetStep(StepY, MinStepY, MaxStepY, Step, Step2, facingUp);
            StepZ = GetStep (StepZ, MinStepZ, MaxStepZ, Step, Step2, facingUp);

            //transform.Translate(Vector3.back * -StepZ);
            transform.Translate(new Vector3(0f, StepY * Time.timeScale, StepZ * Time.timeScale));
        }

        private void MovementY(float y){
            float rotation = y * RotationSpeed * Time.deltaTime;
            //float rotation = CrossPlatformInputManager.GetAxis("Vertical") * RotationSpeed;
            //float rotation = Input.GetAxis("Vertical");

            if (rotation < 0)
				facingUp = true;
			else
				facingUp = false;

            float actualRotation = gameObject.transform.eulerAngles.x;
            
            if ((40 >= actualRotation && actualRotation >= 0) || (360 > actualRotation && actualRotation >= 330)) transform.Rotate(rotation, 0, 0);
            else if (actualRotation > 325 && actualRotation < 330) {
                transform.Rotate(1, 0, 0);
            } else if (actualRotation < 45 && actualRotation > 40) {
                transform.Rotate(-1, 0, 0);
            } 
        }

        private void MovementX(float x) {
            float rotation = x * RotationSpeed * Time.deltaTime;
            //float rotation = CrossPlatformInputManager.GetAxis("Horizontal") * RotationSpeed;
            //float rotation = Input.GetAxis("Horizontal");

            transform.Rotate(0, rotation, 0);
        }

        private float GetStep(float normalStep, float MinStep, float MaxStep, float Step, float Step2, bool isMin) {
            if (isMin) {
                if (normalStep > MinStep) {
                    normalStep -= Step;
                } else {
                    normalStep = MinStep;
                }
            } else {
                if (normalStep < MaxStep) {
                    normalStep += Step2;
                } else {
                    normalStep = MaxStep;
                }
            }

            return normalStep;
        }

        private void Fly() 
        {
            Vector3 position = this.transform.position;
            if (position.y < MaximumHeight) {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.AddRelativeForce(new Vector3(0, StepFly * 100, 0));
                //this.GetComponent<Animator>().Play("fly");
                _gameLogic.Energy(_gameLogic.Energy() - 1);
            }
        }

        private IEnumerator Shoot()
        {
            //this.GetComponent<Animator> ().Play ("bomb");
            shooting = true;

            GameObject poop = GameObject.CreatePrimitive (PrimitiveType.Sphere);
	
            Physics.IgnoreCollision (GetComponent<Collider> (), poop.GetComponent<SphereCollider> ());
            poop.tag = "Poop";
            poop.name = "Poop";
            poop.transform.localScale = _scaleVector;
            poop.transform.position = GetPositionPoop();
            poop.transform.rotation = transform.rotation;

            Rigidbody rb = poop.AddComponent<Rigidbody> ();
            rb.AddRelativeForce(new Vector3(0f, -1200f, 10000));
            rb.useGravity = true;

            poop.transform.parent = _parentPoop.transform;

            poop.AddComponent<Poop> ();
            _gameLogic.Score(_gameLogic.Score() + 1);
            _gameLogic.Energy(_gameLogic.Energy() - 3);

            yield return new WaitForSeconds(WaitShoot);

            shooting = false;
        }

        void OnTriggerEnter(Collider collider) {
            string currentTag = collider.gameObject.tag;
            if (currentTag == "Fruit" || currentTag == "FruitDeluxe") {
                var energy = currentTag == "Fruit" ? 15 : 30;

                if (_gameLogic.Energy() < _gameLogic.MaxEnergy()) {
                    _gameLogic.Energy(_gameLogic.Energy() + energy);
                }

                //_gameLogic.CantFruitsEaten += 1;
                Destroy(collider.gameObject);
            }
        }

        void OnCollisionEnter(Collision collision) {
            string currentTag = collision.gameObject.tag;
            if (collision.gameObject.transform.parent != null && currentTag.Equals("Untagged")) {
                currentTag = collision.gameObject.transform.parent.tag;
            }
            if (currentTag.Equals("Building") || currentTag.Equals("SideWalk")) {
                _gameLogic.Energy(_gameLogic.Energy() - 5);
            } else if (currentTag.Equals("Person")){
                _gameLogic.GameOver = true;
            } else if (currentTag.Equals("Rock")) {
                _gameLogic.Energy(_gameLogic.Energy() - 15);
            }
        }

        private void calibrarteAccelerometer() {
            wantedDeadZone = Input.acceleration;
            Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0f, 0f, -1f), wantedDeadZone);
            //create identity matrix ... rotate our matrix to match up with down vec
            Matrix4x4 matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
            //get the inverse of the matrix
            calibrationMatrix = matrix.inverse;
        }

        private Vector3 getAccelerometer(Vector3 accelerator) {
            return calibrationMatrix.MultiplyVector(accelerator);
        }

        private Vector3 GetPositionPoop(){
            return new Vector3 (transform.position.x, transform.position.y - 3, transform.position.z);
        }

    }
}
