﻿using System;
using Assets.Scripts.LogicScripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UiLogic : MonoBehaviour
    {
        //http://www.1001fonts.com/comic-fonts.html

        public Canvas GameUi;
        public Image PausePanel;
        public Image WarningImage;
        public Image EndImage;

        public Button PauseButton;
        public Button PlayButton;
        public Button ReloadButton;
        public Button MenuButton;
        public Button OkButton;
        public Button CancelButton;

        public Text GameOverLabel;
        public Button ReloadButton1;
        public Button MenuButton1;

        public Text TimerLabel;
        public Text ScoreLabel;
        public Slider EnergySlider;

        private GameLogic _game;

        private float _remainingTime;
        private bool _restart;
        private Image[] _components, _menuItems, _warningItems, _endItems;

        void Start()
        {
            _game = GameLogic.Instance;
            _remainingTime = _game.Time();
            EnergySlider.maxValue = _game.MaxEnergy();
            _components = GameUi.GetComponentsInChildren<Image>();
            _menuItems = PausePanel.GetComponentsInChildren<Image>();
            _warningItems = WarningImage.GetComponentsInChildren<Image>();
            _endItems = EndImage.GetComponentsInChildren<Image>();
            PauseButton.onClick.AddListener(PauseMenu);
            PlayButton.onClick.AddListener(UnPause);
            ReloadButton.onClick.AddListener(Reload);
            MenuButton.onClick.AddListener(GoToMenu);
            ReloadButton1.onClick.AddListener(Reload1);
            MenuButton1.onClick.AddListener(GoToMenu1);
            OkButton.onClick.AddListener(LeaveOk);
            CancelButton.onClick.AddListener(LeaveCancel);
        }

        void Update()
        {
            UpdateTime();
            UpdateScore();
            UpdateEnergy();
            UpdateGameOver();
        }

        private void UpdateTime()
        {
            _remainingTime -= Time.deltaTime;

            var minutes = _remainingTime / 60; //Divide the guiTime by sixty to get the minutes.
            var seconds = _remainingTime % 60;//Use the euclidean division for the seconds.
            //var fraction = (time * 100) % 100;

            //update the label value
            //timerLabel.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);

            TimerLabel.text = string.Format("{0:0}:{1:00}", minutes, seconds);
        }

        private void UpdateScore()
        {
            ScoreLabel.text = _game.Score().ToString();
        }

        private void UpdateEnergy()
        {
            EnergySlider.value = _game.Energy();
        }

        private void UpdateGameOver()
        {
            _game.GameOver = _remainingTime <= 0 || _game.Energy() < 1;
            if (_game.GameOver)
            {
                _game.Finish();
                if (_game.Succeed())
                {
                    SceneManager.LoadScene(2);
                }
                else
                {
                    _warningItems = WarningImage.GetComponentsInChildren<Image>();
                    ShowEnd();
                }
            }
        }

        private void PauseMenu()
        {
            foreach (var component in _components)
            {
                component.raycastTarget = false;
            }

            foreach (var component in _menuItems)
            {
                component.gameObject.SetActive(true);
            }
            
            Time.timeScale = 0f;
        }

        private void UnPause()
        {
            foreach (var component in _components)
            {
                component.raycastTarget = true;
            }

            foreach (var component in _menuItems)
            {
                component.gameObject.SetActive(false);
            }
            
            Time.timeScale = 1f;
        }

        private void Reload()
        {
            foreach (var component in _menuItems)
            {
                component.gameObject.SetActive(false);
            }
            
            PausePanel.gameObject.SetActive(true);
            foreach (var component in _warningItems)
            {
                component.gameObject.SetActive(true);
            }
            _restart = true;
        }

        private void GoToMenu()
        {
            foreach (var component in _menuItems)
            {
                component.gameObject.SetActive(false);
            }

            PausePanel.gameObject.SetActive(true);
            foreach (var component in _warningItems)
            {
                component.gameObject.SetActive(true);
            }
            _restart = false;
        }

        private void Reload1()
        {
            _restart = true;
            LeaveOk();
        }

        private void GoToMenu1()
        {
            _restart = false;
            LeaveOk();
        }
        

        private void LeaveOk()
        {
            if (_restart) _game.Reset();
            Time.timeScale = 1f;
            SceneManager.LoadScene(_restart ? 1 : 0);
        }

        private void LeaveCancel()
        {
            foreach (var component in _menuItems)
            {
                component.gameObject.SetActive(true);
            }

            foreach (var component in _warningItems)
            {
                component.gameObject.SetActive(false);
            }
        }

        private void ShowEnd()
        {
            foreach (var component in _components)
            {
                component.raycastTarget = false;
            }

            foreach (var component in _endItems)
            {
                component.gameObject.SetActive(true);
            }

            Time.timeScale = 0f;
        }
    }
}