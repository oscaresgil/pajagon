﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.LogicScripts;

namespace Assets.Scripts
{
    public enum Tags {Street, Building, SideWalk};

    public class StageLoad : MonoBehaviour
    {
        public float speed = 10f;
        public int cantFruits = 100;
        public int cantPersons = 80;
        public int cantCars = 70;
        public int StageNumber;

        private float separation = 0;

        private Vector3 sizeOfBuild = new Vector3(30f, 20f, 30f);
        private Vector3 sizeOfPersons = new Vector3(10f, 10f, 10f);
        private Vector3 sizeOfCars = new Vector3(10f, 10f, 10f);

        private System.Random ran = new System.Random();
        private int width, height;

        private GameObject parentPerson, parentFruit, parentCars;

        void Start() {
            GameLogic game = GameLogic.Instance;
            StageNumber = game.StageNumber();

            if (StageNumber == 1) {
                cantPersons = 20;
                cantCars = 10;
                cantFruits = 50;
            } else if (StageNumber == 2) {
                cantPersons = 15;
                cantCars = 8;
                cantFruits = 40;
            } else if (StageNumber == 3) {
                cantPersons = 10;
                cantCars = 6;
                cantFruits = 20;
            }

            int[] cityIntegers = GetCityIntegerFromPath("stage" + StageNumber + "");
            int[] citySideWalk = GetPartsFromCity(cityIntegers, 4);
            int[] cityStreet = GetPartsFromCity(cityIntegers, 1);
            GameObject[] city = GetCityFromPath(cityIntegers, StageNumber);

            parentFruit = GenerateFruits (city, cantFruits);
            parentPerson = GeneratePersons (city, citySideWalk, cantPersons);
            if (StageNumber != 2) parentCars = GenerateCars (city, cityStreet, cantCars);

            CreateFloor();
        }

        private GameObject CreateFloor() {
            float SizeOfWidth = width * sizeOfBuild.x;
            float SizeOfDepth = height * sizeOfBuild.z;

            UnityEngine.Object prefab = Resources.Load("Street/Floor", typeof(GameObject));
            GameObject clone = Instantiate(prefab, new Vector3(SizeOfWidth/2f, -1f, SizeOfDepth/2f), Quaternion.identity) as GameObject;

            Vector3 scale = CalculateScale(clone, new Vector3(SizeOfWidth, 1f, SizeOfDepth));
            clone.transform.localScale = scale;
            clone.name = "Floor";

            return clone;
        }

        private int[] GetCityIntegerFromPath(string cityName) {
            TextAsset file = Resources.Load<TextAsset>(cityName);
            string[] stageLines = file.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            width = stageLines[0].Split(',').Length;
            height = stageLines.Length;

            int[] cityInts = new int[height * width];
            for (int j = 0; j < height; j++) {
                string[] lineValues = stageLines[j].Split(',');
                for (int i = 0; i < width; i++) {
                    int position = width * j + i;
                    cityInts[position] = Int32.Parse(lineValues[i]);
                }
            }

            return cityInts;

        }

        private int[] GetPartsFromCity(int[] cityInts, int numToFind) {
            List<int> positionsOfStreet = new List<int>();
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    int position = width * j + i;
                    if (cityInts[position] == numToFind) {
                        positionsOfStreet.Add(position);
                    }
                }
            }

            return positionsOfStreet.ToArray();
        }

        private GameObject[] GetCityFromPath(int[] cityInts, int typeCity) {
            GameObject parentCity = new GameObject("City");
            GameObject[] city = new GameObject[
                height * width
                ];

            bool normalView = false;

            float max_total_width = 0;
            for (int j = 0; j < height; j++) {
                int actualJ = -1;

                for (int i = 0; i < width; i++) {
                    int actualPosition = width * j + i;
                    int actualPositionRight = width * j + (i + 1);
                    int actualPositionLeft = width * j + (i - 1);
                    int actualPositionDown = width * (j + 1) + i;
                    int actualPositionUp = width * (j - 1) + i;

                    String tagName = "";
                    String name = "";
                    UnityEngine.Object prefab = null;
                    int type = cityInts[actualPosition];

                    if (type == 0 || type == 8) { // Edificio
                        tagName = "Building";
                        if (typeCity == 1 || typeCity == 3) prefab = Resources.Load("Buildings/" + ran.Next(1, 25) + "", typeof(GameObject));
                        else {
                            if (type == 0) {
                                prefab = Resources.Load("Park/Trees/" + ran.Next(1, 7) + "", typeof(GameObject));
                            } else {
                                prefab = Resources.Load("Park/Trees/Simple/" + ran.Next(1, 7) + "", typeof(GameObject));
                            }
                        }
                    } else if (type == 1) { // Carretera
                        tagName = "Street";
                        name = "Street";
                        if (typeCity == 1 || typeCity == 3) prefab = Resources.Load("Street/Street", typeof(GameObject));
                        else prefab = Resources.Load("Park/Bushes/" + ran.Next(1, 5) + "", typeof(GameObject));
                    } else if (type == 2) { // Union
                        tagName = "Street";
                        int buttomPosition = width * (j + 3) + i;
                        if (buttomPosition < city.Length) {
                            int buttonNumber = cityInts[buttomPosition];
                            int rightNumber = cityInts[actualPositionRight];
                            if (buttonNumber == 2 && rightNumber == 2) {
                                prefab = Resources.Load("Street/Street+", typeof(GameObject));
                            }
                        }
                        name = "Intersection";
                    } else if (type == 4) { // Banqueta
                        tagName = "SideWalk";
                        name = "SideWalk_0";
                        if (typeCity == 1 || typeCity == 3) {
                            prefab = Resources.Load("Street/SideWalk", typeof(GameObject));
                        } else {
                            prefab = Resources.Load("Street/SideWalkRoad", typeof(GameObject));
                        }
                    } else if (type == 5) { // Arbustos
                        tagName = "Building";
                        prefab = Resources.Load("Park/Bushes/Simple/" + ran.Next(1, 5) + "", typeof(GameObject));
                    } else if (type == 6) {
                        tagName = "Barrier";
                        name = "Barrier";
                        prefab = Resources.Load("Street/Barrier", typeof(GameObject));
                    } else { // Algo Mas
                        prefab = null;
                    }

                    GameObject clone = null;

                    if (tagName == "Building") {
                        if (j < height && i < width) {
                            int rightNumber = cityInts[actualPositionRight];
                            int downNumber = cityInts[actualPositionDown];
                            int leftNumber = cityInts[actualPositionLeft];
                            int doubleLeft = cityInts[width * j + (i - 2)];
                            int doubleRight = cityInts[width * j + (i + 2)];

                            if (rightNumber == 0 && downNumber == 0 && (leftNumber != 0 || doubleRight != 0) || typeCity == 2) {// && doubleLeft != 0) {
                                clone = createObject(prefab, tagName, parentCity);

                                Vector3 scaleObject = CalculateScale(clone, sizeOfBuild);

                                if (typeCity == 1 || typeCity == 3) {
                                    clone.transform.localScale = new Vector3(scaleObject.x + 3f, sizeOfBuild.y, scaleObject.z + 6f);
                                    clone.transform.position = new Vector3(i * sizeOfBuild.x + sizeOfBuild.x / 2f, 0f, j * sizeOfBuild.z + sizeOfBuild.z / 2f);
                                } else {
                                    clone.transform.localScale = new Vector3(scaleObject.x, sizeOfBuild.y, scaleObject.z);
                                    clone.transform.position = new Vector3(i * sizeOfBuild.x , -2f, j * sizeOfBuild.z);
                                }

                                if (actualJ != j) {
                                    normalView = !normalView;
                                    actualJ = j;
                                }

                                if (!normalView) {
                                    clone.transform.Rotate(0f, 180f, 0f);
                                }
                            }
                        }
                    } else {
                        if (prefab != null) {
                            clone = createObject(prefab, tagName, parentCity);
                            clone.name = name;

                            Vector3 scaleObject = CalculateScale(clone, sizeOfBuild);
                            bool alreadyScaled = false;

                            scaleObject.y = clone.transform.localScale.y;
                            if (tagName.Equals("Street")) {
                                if (type != 2) {
                                    int rightNumber = cityInts[actualPositionRight];
                                    int leftNumber = cityInts[actualPositionLeft];
                                    if (actualPositionUp > 0 && actualPositionDown < city.Length) {
                                        int downNumber = cityInts[actualPositionDown];
                                        int upNumber = cityInts[actualPositionUp];

                                        if ((rightNumber == 1 || leftNumber == 1)
                                            &&
                                            ((downNumber != 1 && downNumber != 2 && downNumber != 6) || (upNumber != 1 && upNumber != 2 && upNumber != 6))) {
                                            clone.transform.Rotate(0, 90f, 0);
                                            if (downNumber == 1) {
                                                clone.name = name + "_R_L";
                                            } else {
                                                clone.name = name + "_R_R";
                                            }
                                        } else {
                                            if (rightNumber == 1) {
                                                clone.name = name + "_L_U";
                                            } else {
                                                clone.name = name + "_L_D";
                                            }
                                        }
                                    }

                                } else {
                                    clone.transform.position = new Vector3(i * sizeOfBuild.x + sizeOfBuild.x / 2f, 0.5f, j * sizeOfBuild.z + sizeOfBuild.z * 1.5f);
                                    scaleObject = new Vector3(scaleObject.x * 5.5f, sizeOfBuild.y, scaleObject.z * 5.5f);
                                    alreadyScaled = true;
                                }

                            } else if (tagName.Equals("Barrier")) {
                                if (j == 0 || j == height - 1) {
                                    if (j == 0) {
                                        //clone.transform.Rotate(90f, 0f, 0f);
                                        clone.transform.Rotate(270f, 0f, 0f);
                                    } else {
                                        //clone.transform.Rotate(270f, 0f, 0f);
                                        clone.transform.Rotate(90f, 0f, 0f);
                                    }
                                    scaleObject = new Vector3(scaleObject.x, scaleObject.y, sizeOfBuild.z * 2f);
                                } else {
                                    if (i == 0) {
                                        //clone.transform.Rotate(0f, 0f, 270f);
                                        clone.transform.Rotate(0f, 0f, 90f);
                                    } else {
                                        //clone.transform.Rotate(0f, 0f, 90f);
                                        clone.transform.Rotate(0f, 0f, 270f);
                                    }

                                    scaleObject = new Vector3(sizeOfBuild.x * 2f, scaleObject.y, scaleObject.z);
                                }

                                alreadyScaled = true;
                                clone.transform.position = new Vector3(i * sizeOfBuild.x, sizeOfBuild.x * 10f, j * sizeOfBuild.z);
                            }

                            clone.transform.localScale = scaleObject;

                            if (!alreadyScaled)
                                clone.transform.position = new Vector3(i * sizeOfBuild.x, 0f, j * sizeOfBuild.z);
                        }
                    }

                    city[actualPosition] = clone;

                }

                max_total_width += sizeOfBuild.x;
            }

            return city;
        }

        private GameObject createObject(UnityEngine.Object prefab, String tagName, GameObject parentCity) {
            GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
            clone.transform.parent = parentCity.transform;
            clone.tag = tagName;

            return clone;
        }

        private Vector3 CalculateScale(GameObject clone, Vector3 size) {
            Vector3 finalValue = new Vector3();

            Renderer renderer = clone.GetComponent<Renderer>();
            Vector3 actualSize = new Vector3(0f, 0f, 0f);
            if (renderer != null) {
                actualSize = clone.GetComponent<Renderer>().bounds.size;
            } else {
                actualSize = CalculateActualSize(clone);
            }

            finalValue.x = size.x / actualSize.x;
            finalValue.z = size.z / actualSize.z;

            if (clone.tag.Equals("Building") || clone.name.Equals("Floor")) {
                finalValue.x = (actualSize.x * 100) / size.x;
                finalValue.z = (actualSize.z * 100) / size.z;
            }

            return finalValue;
        }

		private Vector3 CalculateActualSize(GameObject clone){
			Renderer renderer = clone.GetComponent<Renderer> ();
			Vector3 actualSize = new Vector3(0f, 0f, 0f);
			if (renderer != null) {
				actualSize = renderer.bounds.size;
			} else {
				foreach (Transform child in clone.transform) {
					renderer = child.gameObject.GetComponent<Renderer> ();
					if (renderer != null) {
						if (actualSize.x < renderer.bounds.size.x) {
							actualSize.x = renderer.bounds.size.x;	
						}

						actualSize.y += renderer.bounds.size.y;

						if (actualSize.z < renderer.bounds.size.z) {
							actualSize.z = renderer.bounds.size.z;
						}
					}
				}
			}
            
			return actualSize;
		}
		
        private GameObject GenerateFruits(GameObject[] gameObjects, int cantFruits){
            GameObject parentFruits = new GameObject ("Fruits");

            for (int i = 0; i < cantFruits; i++) {

				String tag = "FruitDeluxe";
                int number = ran.Next(1, 4);
                UnityEngine.Object prefab = Resources.Load("Fruits/" + number + "", typeof(GameObject));

				GameObject fruit = Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject;
				fruit.AddComponent<Fruit> ();

                int randomDeluxe = ran.Next(0, 50);
                if (randomDeluxe > 5) { // NOT DELUXE
                    tag = "Fruit";
                    Behaviour halo = (Behaviour) fruit.GetComponent("Halo");
                    halo.enabled = false;
                }

				fruit.tag = tag;
                fruit.transform.parent = parentFruits.transform;
				fruit.name = "Fruit_"+i;

                int randomX = ran.Next (0, width);
                int randomY = ran.Next (0, height);

                Vector3 size = CalculateActualSize(fruit);

                /*float randomWidth = Convert.ToSingle(GetRandomNumber(-size.x / 2f, size.x / 2f));
                float randomHeight = Convert.ToSingle(GetRandomNumber(size.y, size.y * 2f));
                float randomDepth = Convert.ToSingle(GetRandomNumber(-size.z / 2f, size.z / 2f));*/

                float randomWidth = Convert.ToSingle(GetRandomNumber(-size.x, size.x));
                float randomHeight = Convert.ToSingle(GetRandomNumber(size.y, size.y * 2f));
                float randomDepth = Convert.ToSingle(GetRandomNumber(-size.z, size.z));

                GameObject actualObject = gameObjects [width * randomY + randomX];
                if (actualObject != null) {
                    Vector3 actualPosition = actualObject.transform.position;
                    fruit.transform.position = new Vector3 (actualPosition.x + randomWidth, actualPosition.y + randomHeight, actualPosition.z + randomDepth);
                } else {
                    fruit.transform.position = new Vector3((width * randomX) + randomWidth, sizeOfBuild.y + randomHeight, (width * randomY) + randomDepth);
                }
            }

            return parentFruits;
        }

        private GameObject GeneratePersons(GameObject[] gameObjects, int[] sideWalks, int cantPersons){
            GameObject parentPersons = new GameObject ("Persons");
	
            for (int i = 0; i < cantPersons; i++) {
                int sideWalk = ran.Next (0, sideWalks.Length);

                GameObject actualObject = gameObjects [sideWalks[sideWalk]];
                Vector3 actualPosition = actualObject.transform.position;

                float randomWidth = Convert.ToSingle (GetRandomNumber (-sizeOfBuild.x / 2f, sizeOfBuild.x / 2f));
                float randomDepth = Convert.ToSingle (GetRandomNumber (-sizeOfBuild.z / 2f, sizeOfBuild.z / 2f));

                int number = ran.Next(1, 37);
                UnityEngine.Object prefab = Resources.Load("People/" + number + "", typeof(GameObject));
                //UnityEngine.Object prefab = Resources.Load("People/" + 1 + "", typeof(GameObject));
                GameObject person = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                person.AddComponent<Person>();
                person.transform.parent = parentPersons.transform;
                person.transform.localScale = sizeOfPersons;
                person.tag = "Person";

                // Si el sidewalk esta a direccion hacia abajo..
                int directionFacing = ran.Next(0, 1);
                Vector3 rotatingPerson;
                if (actualObject.name.Equals("SideWalk_1")) {
                    if (directionFacing == 1) {
                        rotatingPerson = new Vector3(0f,270f,0f);
                    } else {
                        rotatingPerson = new Vector3(0f,90f,0f);
                    }
                } else { // De otra forma
                    if (directionFacing == 1) {
                        rotatingPerson = new Vector3(0f,0f,0f);
                    } else {
                        rotatingPerson = new Vector3(0f,180f,0f);
                    }
                }

                person.transform.eulerAngles = rotatingPerson;

                //Vector3 personSize = person.GetComponent<Renderer> ().bounds.size;
                person.transform.position = new Vector3 (actualPosition.x + randomWidth, 0f, actualPosition.z + randomDepth);
            }

            return parentPersons;
        }

        private GameObject GenerateCars(GameObject[] gameObjects, int[] streets, int cantCars){
            GameObject parentCars = new GameObject ("Cars");

            for (int i = 0; i < cantCars; i++) {
                int street = ran.Next (0, streets.Length);

                GameObject actualObject = gameObjects [streets [street]];
                Vector3 actualPosition = actualObject.transform.position;
                                
                UnityEngine.Object prefab = Resources.Load("Cars/" + ran.Next(1, 10) + "", typeof(GameObject));
                GameObject car = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
                car.transform.parent = parentCars.transform;
                car.transform.localScale = sizeOfCars;
                car.tag = "Car";

                //Vector3 carSize = car.GetComponent<Renderer> ().bounds.size;
                Vector3 carSize = CalculateActualSize(car);
                car.transform.position = new Vector3 (actualPosition.x, 0f, actualPosition.z);

                car.AddComponent<BoxCollider>();
                car.GetComponent<BoxCollider>().size = carSize / 12f;

                car.AddComponent<Car>();

                String name = actualObject.name;
                String carName = "Car_R_U";
                if (name.Equals("Street_R_R")) {
                    car.transform.Rotate(0f, 270f, 0f);
                    carName = "Car_R_R";
                } else if (name.Equals("Street_R_L")) {
                    car.transform.Rotate(0f, 90f, 0f);
                    carName = "Car_R_L";
                } else if (name.Equals("Street_L_D")) {
                    car.transform.Rotate(0f, 180f, 0f);
                    carName = "Car_L_D";
                }

                car.name = carName;
            }

            return parentCars;
        }

        public double GetRandomNumber(double minimum, double maximum){ 
            return ran.NextDouble() * (maximum - minimum) + minimum;
        }

    }
}