﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FollowCamera : MonoBehaviour {

        public Transform target; // Target to apply follow
        float distance = 10.0f, // Distance to target
            height = 5.0f, // Value of height
            heightDamping = 1.0f, // Value of smooth height transition
            rotationDamping = 3.0f; // Value of smooth angle transition

        void LateUpdate() {
            // Calculate actual and desirable rotation angles
            float currentRotationAngleX = transform.eulerAngles.x;
            float wantedRotationAngleX = target.eulerAngles.x;
            float currentRotationAngleY = transform.eulerAngles.y;
            //float wantedRotationAngleY = target.eulerAngles.y + 180; // +180 asset corrupt
			float wantedRotationAngleY = target.eulerAngles.y;

            // Calculate actual and desirable height value
            float currentHeight = transform.position.y;
            float wantedHeight = target.position.y + height;

            // Damp rotation in x
            // Move current rotation in x interpolating from current to wanted angle, to make it smooth
            //currentRotationAngleX = Mathf.LerpAngle(currentRotationAngleX, -wantedRotationAngleX, rotationDamping * Time.deltaTime); // -wantedRotation asset corrupt
			currentRotationAngleX = Mathf.LerpAngle(currentRotationAngleX, wantedRotationAngleX, rotationDamping * Time.deltaTime);
            // Damp rotation in y
            currentRotationAngleY = Mathf.LerpAngle(currentRotationAngleY, wantedRotationAngleY, rotationDamping * Time.deltaTime);

            // Damp height
            currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

            // Convert angle to rotation
            Quaternion currentRotation = Quaternion.Euler(currentRotationAngleX, currentRotationAngleY, 0);

            // Se position of camera on x-z plane in distance behind the target
            transform.position = target.position;
            transform.position -= currentRotation * Vector3.forward * distance;

            transform.rotation = target.rotation;

            // Sets position
            //setPositionX(currentHeight);
            //setPositionY(currentHeight);

            // Look always at target
            transform.LookAt(target);
        }

        private void setPositionX(float x) {
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }

        private void setPositionY(float y) {
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }

    }
}
