﻿using Assets.Scripts.LogicScripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ScoreLogic : MonoBehaviour {

        public Image Star1;
        public Image Star2;
        public Image Star3;

        public Sprite Star1Sprite;
        public Sprite Star2Sprite;
        public Sprite Star3Sprite;

        public Text ScoreLabel;
        public Text BestScoreLabel;

        public Button MenuButton;
        public Button ReloadButton;
        public Button ForwardButton;

        private GameLogic _game;

        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            _game = GameLogic.Instance;

            MenuButton.onClick.AddListener(Menu);
            ReloadButton.onClick.AddListener(Reload);
            ForwardButton.onClick.AddListener(Forward);
            Stats();
        }

        private void Stats()
        {
            ScoreLabel.text = "Score:\t" + _game.Score();
            BestScoreLabel.text = "Best Score:\t" + _game.BestScore();
            int stars = _game.GetStars(_game.Score());
            switch (stars)
            {
                case 1:
                    Star1.sprite = Star1Sprite;
                    break;
                case 2:
                    Star2.sprite = Star2Sprite;
                    goto case 1;
                case 3:
                    Star1.sprite = Star1Sprite;
                    goto case 2;
            }
        }

        private void Menu()
        {
            _game.Reset();
            SceneManager.LoadScene(0);
        }

        private void Reload()
        {
            _game.Reset();
            SceneManager.LoadScene(1);
        }

        private void Forward()
        {
            if (_game.StageNumber() != 3)
            {
                _game.SetStage(_game.StageNumber() + 1);
            }
            else
            {
                _game.SetStage(1);
            }
            
            SceneManager.LoadScene(1);
        }

    }
}
