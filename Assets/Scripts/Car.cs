﻿using System;
using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour {

    public float MaximumDistance = 70f;
    public float MaxVelocity = 1.5f;
    public float WaitTime = 1.5f;

    private float Velocity = 1.5f;
    private Vector3 sizeOfBuild = new Vector3(30f, 20f, 30f);

    private bool IsHitted = false;
    private RaycastHit hit; 
    private float StartWaitingTime;

    void Start() {
        StartWaitingTime = WaitTime;
    }

    void Update() {
        if (!IsHitted) Velocity = CalculateVelocity();

        hit = new RaycastHit();
        if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, MaximumDistance)) {
            if (Math.Abs(StartWaitingTime - WaitTime) < 0.01 || Time.time - StartWaitingTime >= WaitTime) {
                transform.Translate(Vector3.forward * Velocity * Time.timeScale);
            }
        } else {
            StartWaitingTime = Time.time;

            string name = hit.collider.name;
            string actualName = gameObject.name;
            string tag = GetGameObjectTag(hit.collider.gameObject);

            if (name.Equals("Barrier")) {
                if (actualName.Equals("Car_L_D")) {
                    gameObject.name = "Car_R_U";
                    gameObject.transform.Rotate(0f, 180f, 0f);
                    gameObject.transform.Translate(-sizeOfBuild.x, 0f, 0f);
                } else if (actualName.Equals("Car_R_U")) {
                    gameObject.name = "Car_L_D";
                    gameObject.transform.Rotate(0f, 180f, 0f);
                    gameObject.transform.Translate(-sizeOfBuild.x, 0f, 0f);
                } else if (actualName.Equals("Car_R_L")) {
                    gameObject.name = "Car_R_R";
                    gameObject.transform.Rotate(0f, 180f, 0f);
                    gameObject.transform.Translate(sizeOfBuild.x, 0f, 0f);
                } else if (actualName.Equals("Car_R_R")) {
                    gameObject.name = "Car_R_L";
                    gameObject.transform.Rotate(0f, 180f, 0f);
                    gameObject.transform.Translate(sizeOfBuild.z, 0f, 0f);
                }
                StartWaitingTime = WaitTime;
                Velocity = MaxVelocity;
                IsHitted = false;

            } else if (tag.Equals("Fruit")) {
                transform.Translate(Vector3.forward * Velocity);
                StartWaitingTime = WaitTime;

            } else if (tag.Equals("Car")) {
                if (!IsHitted) Velocity = 0.5f;
                if (name.Equals("Car_R_U") || name.Equals("Car_R_D")) {
                    if (Time.time - StartWaitingTime >= WaitTime) {
                        transform.Translate(Vector3.forward * Velocity);
                    }
                }

            } else {
                if (!IsHitted) Velocity = 0.5f;
            }
        }
    }

    private string GetGameObjectTag(GameObject rayObject) {
        string tag = rayObject.tag;
        if (rayObject.transform.parent != null && tag.Equals("Untagged")) {
            tag = rayObject.transform.parent.tag;
        }
        return tag;
    }

    private float CalculateVelocity() {
        if (Velocity < MaxVelocity) {
            Velocity += 0.3f * Time.deltaTime;
        } else {
            Velocity = MaxVelocity;
        }
        return Velocity;
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.collider.name.Equals("Poop")) {
            Velocity = 10f;
            IsHitted = true;
        }
    }
}
