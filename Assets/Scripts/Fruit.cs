﻿using UnityEngine;
using System.Collections;

public class Fruit : MonoBehaviour {

	private float rotation = 50f;

    void Start() {
        GetComponent<Collider>().isTrigger = true;
    }

	void Update () {
		transform.Rotate (0f, Time.deltaTime * rotation, 0f);
	}
}
