﻿
namespace Assets.Scripts.LogicScripts
{
    public class Stage {

        public int StageNumber;
        public float Time;
        public int MaxEnergy = 200;
        public int Energy;
        public float DefaultTime;
        public int DefaultEnergy;

        // Score
        public int Score;
        public int BestScore;

        // Scores for stars
        private readonly int _minScore;
        private readonly int _avgScore;
        private readonly int _goodScore;

        /*public int CantFruitsEaten;
        public int CantPersonsPooped;
        public int CantCarsPooped;*/

        public Stage(int stageNumber, float time, int energy, int minScore, int avgScore, int goodScore)
        {
            StageNumber = stageNumber;
            Time = time;
            Energy = energy;
            DefaultEnergy = energy;
            DefaultTime = time;

            _minScore = minScore;
            _avgScore = avgScore;
            _goodScore = goodScore;
        }

        public bool Succeed()
        {
            return Score >= _minScore;
        }
        
        public void Reset()
        {
            Time = DefaultTime;
            Energy = DefaultEnergy;
            /*CantFruitsEaten = 0;
            CantPersonsPooped = 0;
            CantCarsPooped = 0;*/
            Score = 0;
        }

        public void Finish()
        {
            BestScore = Score;
        }

        // Finish game and get number of stars
        public int GetStars(int score)
        {
            Score = score;
            if (BestScore < score) BestScore = score;

            int stars = 0;
            if (score > _minScore)
            {
                stars = 1;
            }
            else if (score > _avgScore)
            {
                stars = 2;
            }
            else if (score > _goodScore)
            {
                stars = 3;
            }
            return stars;
        }
    }

}
