﻿using JetBrains.Annotations;

namespace Assets.Scripts.LogicScripts{
	public class GameLogic : Singleton<GameLogic>
	{

	    public bool InMenu = false;

        public string PlayerName { get; set; }
		public int DificultyLevel { get; set; }

	    private Stage _currentStage;

        private readonly Stage _level1Stage = new Stage(1, 40f, 50, 10, 15, 25);
        private readonly Stage _level2Stage = new Stage(2, 90f, 100, 100, 200, 300);
        private readonly Stage _level3Stage = new Stage(3, 90f, 75, 100, 200, 300);

        public bool GameOver { get; set; }

	    [UsedImplicitly]
	    void Start(){
			DontDestroyOnLoad(this);
	        _currentStage = _level1Stage;
	    }

	    public void SetStage(int stage)
	    {
	        if (stage == 1) _currentStage = _level1Stage;
	        if (stage == 2) _currentStage = _level2Stage;
	        if (stage == 3) _currentStage = _level3Stage;
	    }

	    public void Reset()
	    {
	        _currentStage.Reset();
	    }

	    public void Finish()
	    {
	        _currentStage.Finish();
	    }

	    public int GetStars(int score)
	    {
	        return _currentStage.GetStars(score);
	    }

	    public int StageNumber()
	    {
	        return _currentStage.StageNumber;
	    }

        // Time
	    public float Time()
	    {
	        return _currentStage.Time;
	    }

	    public void Time(float time)
	    {
	        _currentStage.Time = time;
	    }

        // Score
	    public int Score()
	    {
	        return _currentStage.Score;
	    }

	    public void Score(int score)
	    {
	        _currentStage.Score = score;
	    }

        // Energy
        public int Energy()
        {
            return _currentStage.Energy;
        }

        public void Energy(int energy)
        {
            _currentStage.Energy = energy;
        }

	    public int MaxEnergy()
	    {
	        return _currentStage.MaxEnergy;
	    }

        // Max Score
	    public int BestScore()
	    {
	        return _currentStage.BestScore;
	    }

	    public bool Succeed()
	    {
	        return _currentStage.Succeed();
	    }


        /*public int CantFruitsEaten;
        public int CantPersonsPooped;
        public int CantCarsPooped;*/

	}
}
